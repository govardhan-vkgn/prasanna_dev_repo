package Waits;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;

public class Locators {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demoqa.com/automation-practice-form");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		driver.findElement(By.id("firstName")).sendKeys("prasanna");
		Thread.sleep(2000);
		driver.findElement(By.id("firstName")).clear();
		Thread.sleep(2000);
		driver.findElement(By.id("firstName")).sendKeys("prasanna");
		Thread.sleep(2000);
		// radio button
		driver.findElement(By.xpath("//label[text()='Male']")).click();
		boolean state = driver.findElement(By.name("gender")).isSelected();
		System.out.println(state);
		// dropdown
		driver.findElement(By.id("dateOfBirthInput")).click();
		WebElement drpdwn = driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"));
		Select se = new Select(drpdwn);
		se.selectByVisibleText("April");
		WebElement drpdwn1 = driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"));
		Select se1 = new Select(drpdwn1);
		se1.selectByValue("1901");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='react-datepicker__day react-datepicker__day--004']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@title='Ad.Plus Advertising']")).click();
		driver.findElement(By.xpath("//label[text()='Sports']")).click();
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).submit();

	}

}
