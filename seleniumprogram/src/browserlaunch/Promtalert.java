package browserlaunch;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Promtalert {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		 driver.get("https://demoqa.com/alerts");
		   driver.manage().window().maximize();
		
		driver.findElement(By.id("promtButton")).click();
		String t=driver.switchTo().alert().getText();
		System.out.println(t);
		driver.switchTo().alert().sendKeys("prasanna");
		
		driver.switchTo().alert().accept();
		System.out.println(driver.findElement(By.id("promptResult")).getText());
		
	}

}
