package browserlaunch;

import java.util.ArrayList;
import java.util.Collections;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Dropdownusingarraylist {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.guru99.com/test/newtours/register.php");
		driver.manage().window().maximize();
		driver.findElement(By.name("country")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//option[text()='INDIA']")).click();
		Thread.sleep(3000);
		WebElement drpdwn = driver.findElement(By.name("country"));
		Select se = new Select(drpdwn);
		se.selectByIndex(10);
		Thread.sleep(3000);
	// se.deselectByIndex(10);
		//Thread.sleep(3000);
		se.selectByVisibleText("INDIA");
		Thread.sleep(3000);
		// se.deselectByVisibleText("INDIA");
		// Thread.sleep(3000);
		se.selectByValue("ANGUILLA");
		Thread.sleep(3000);
		// se.deselectByValue("ANGUILLA");
		// Thread.sleep(3000);
		ArrayList<String> ar = new ArrayList<>();
		for (WebElement e : se.getOptions()) {
			ar.add(e.getText());			
		}	
		System.out.println("All country names" + ar);
		Collections.sort(ar);
		System.out.println(ar);

	}
}
