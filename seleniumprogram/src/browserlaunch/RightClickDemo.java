package browserlaunch;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.By;

import org.openqa.selenium.interactions.Actions;

public class RightClickDemo {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://demoqa.com/buttons");
		driver.manage().window().maximize();
		Actions actions = new Actions(driver);
		WebElement doubleClickBtn = driver.findElement(By.id("doubleClickBtn"));
		actions.doubleClick(doubleClickBtn).build().perform();
		Thread.sleep(5000);
		WebElement rightClickBtn = driver.findElement(By.id("rightClickBtn"));
		actions.contextClick(rightClickBtn).build().perform();
		Thread.sleep(5000);
	}
}

/*
 * driver.findElement(By.id("doubleClickBtn")).click();
 * System.out.println(driver.findElement(By.id("doubleClickBtn")).getText());
 * Thread.sleep(3000); driver.findElement(By.id("rightClickBtn")).click();
 * System.out.println(driver.findElement(By.id("rightClickBtn")).getText());
 * Thread.sleep(3000);
 */
