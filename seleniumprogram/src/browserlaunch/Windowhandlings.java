package browserlaunch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandlings {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.ebay.com");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElement(By.id("gh-ac")).sendKeys("Apple iPhone XR 128GB");
		driver.findElement(By.id("gh-btn")).click();
		Thread.sleep(5000);
		List<WebElement> list = driver.findElements(By.xpath("//h3[@class='s-item__title']"));
		System.out.println(list.size());
		Thread.sleep(5000);
		for (WebElement tt : list) {
			tt.click();
			Thread.sleep(5000);
			ArrayList<String> ar = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(ar.get(0));
			Thread.sleep(5000);
		}

		Set<String> alltb = driver.getWindowHandles();
		for (String all : alltb) {
			driver.switchTo().window(all);
			System.out.println(driver.getTitle());
			
			

		}
		
	}

}
