package browserlaunch;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Verify_the_product_using_all_categories {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = Baseclass.driverInvoke();
		Baseclass.loginApplication();
		driver.findElement(By.xpath("//span[text()='All categories']")).click();
		driver.findElement(By.xpath("//span[text()='fruits & vegetables']")).click();
		Thread.sleep(5000);
		if (driver.findElement(By.xpath("//h1[text()='Fruits & Vegetables']")).isDisplayed()) {
			System.out.println("fruits and vegetable session is displayed");
		}
		System.out.println("Display all categories");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='Add'][1]")).click();
		System.out.println("the product is addded to cart");
		driver.findElement(By.xpath("//i[@class='fa fa-shopping-bag font-22'][1]")).click();
		System.out.println("open the add to cart");
		Thread.sleep(3000);
		if (driver.findElement(By.xpath("//div[text()='remove']")).isDisplayed() == true) {
			driver.findElement(By.xpath("//div[text()='remove']")).click();
			System.out.println("The cart items are deleted suceesfully");
		}

	}
}
