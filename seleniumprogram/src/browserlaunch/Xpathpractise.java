package browserlaunch;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Xpathpractise {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.hyrtutorials.com/p/add-padding-to-containers.html");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("//h1[contains(text(),'Register')]")).getText());
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("//a[starts-with(text(),'Sign in')]")).getText());
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("//label[normalize-space(text()='First Name')]")).getText());
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("(//table[@id='contactList']/tbody/tr)[last()]")).getText());
		Thread.sleep(2000);
		// System.out.println(driver.findElements(By.xpath("(//table[@id='contactList']/tbody/tr)[position>=2])")).size());
		// Thread.sleep(2000);
		driver.findElement(By.xpath("//label[text()='Email']/following-sibling::input[@type='text']"))
				.sendKeys("a@gmail.com");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//td[text()='Maria Anders']/preceding-sibling::td/child::input")).click();
		System.out.println(driver.findElement(By.xpath("//table[@id='contactList']/child::tbody/child::tr")).getText());
		Thread.sleep(2000);
		driver.findElement(By.xpath("//td[text()='Francisco Chang']/parent::tr")).click();
		System.out.println(driver.findElement(By.xpath("//td[text()='Francisco Chang']/parent::tr")).getText());
		Thread.sleep(2000);
		System.out.println(driver.findElement(By.xpath("//td[text()='Helen Bennett']/ancestor::tr")).getText());
		//driver.findElement(By.xpath("//table[@id='contactList']/decendant::tr"));
		}

}
