package browserlaunch;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ConformationAlert {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/delete_customer.php");
		driver.findElement(By.name("cusid")).sendKeys("54776");
		driver.findElement(By.name("submit")).click();// first click
		Thread.sleep(3000);
		String t = driver.switchTo().alert().getText();// to print the text in popup
		System.out.println(t);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();// to accept
		Thread.sleep(3000);
		driver.switchTo().alert().accept();// to accept to print sccessful pop up go to home page

	}

}
