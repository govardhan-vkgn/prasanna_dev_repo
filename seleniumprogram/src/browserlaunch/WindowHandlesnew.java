package browserlaunch;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlesnew {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://demoqa.com/browser-windows");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElement(By.id("messageWindowButton")).click();
		Set<String> alltabs = driver.getWindowHandles();
		for (String tab : alltabs) {
			driver.switchTo().window(tab);
			System.out.println(driver.getTitle());
		}
		System.out.println("====");
	}
}

/*
 * driver.get("https://demoqa.com/browser-windows");
 * driver.manage().window().maximize(); Thread.sleep(5000);
 * driver.findElement(By.xpath("//button[text()='New Tab']")).click();
 * Thread.sleep(2000); Set<String> alltabs = driver.getWindowHandles(); for
 * (String tab : alltabs) { driver.switchTo().window(tab); Thread.sleep(5000);
 * String title = driver.getTitle(); System.out.println(title);
 * //driver.close(); }
 */

/*
 * driver.get("https://demoqa.com/browser-windows");
 * 
 * // Open new child window within the main window
 * driver.findElement(By.id("windowButton")).click();
 * 
 * // Get handles of the windows String mainWindowHandle =
 * driver.getWindowHandle();
 * 
 * Set<String> allWindowHandles = driver.getWindowHandles();
 * 
 * Iterator<String> iterator = allWindowHandles.iterator();
 * 
 * // Here we will check if child window has other child windows and will fetch
 * the // heading of the child window while (iterator.hasNext()) { String
 * ChildWindow = iterator.next();
 * 
 * if (!mainWindowHandle.equalsIgnoreCase(ChildWindow)) {
 * driver.switchTo().window(ChildWindow); String text =
 * driver.findElement(By.id("sampleHeading")).getText();
 * System.out.println(text); Thread.sleep(5000); driver.close();
 * Thread.sleep(5000); }
 * 
 */
