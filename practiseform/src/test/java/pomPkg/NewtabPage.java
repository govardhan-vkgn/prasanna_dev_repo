package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class NewtabPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public NewtabPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Alerts, Frame & Windows']")
	private WebElement window;

	@FindBy(xpath = "//span[text()='Browser Windows']//parent::li")
	private WebElement BrowserWindows;

	@FindBy(xpath = "//button[text()='New Tab']")
	private WebElement clickonnewtab;

	public void navigatetowindowspage() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(100,500)");
		sf.clickElement(window);
		sf.clickElement(BrowserWindows);
	}

	public void VerifyNewTabisOpened() {
		sf.clickElement(clickonnewtab);
	}
}
