package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class ProgressBarPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public ProgressBarPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;

	@FindBy(xpath = "//span[text()='Progress Bar']")
	private WebElement progressbar;

	
	@FindBy(xpath = "//button[text()='Start']")
	private WebElement startprogressbar;

	@FindBy(xpath = "//button[text()='Stop']")
	private WebElement stopprogressbar;
	
	@FindBy(xpath = "//button[text()='Reset']")
	private WebElement resetprogressbar;

	public void navigateToProgressBarPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,600)");
		sf.clickElement(Widgets);
		sf.clickElement(progressbar);
	}

	public void startProgressBar() throws InterruptedException {
		// TouchActions action=new TouchActions(driver);
		// action.longPress(ElementOption.element(startprogressbar)).moveToElement(ElementOption.element(startprogressbar,
		// 20, 20)).release().perform();
		//Actions action = new Actions(driver);
		//action.moveToElement(startprogressbar, 50, 0).perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()",startprogressbar);
		Thread.sleep(2000);
		js.executeScript("arguments[0].click",stopprogressbar);
		Thread.sleep(2000);
		js.executeScript("arguments[0].click",resetprogressbar);
		Thread.sleep(2000);
	}
}
