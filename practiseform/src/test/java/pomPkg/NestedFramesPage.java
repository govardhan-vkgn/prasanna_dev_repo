package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class NestedFramesPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public NestedFramesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Alerts, Frame & Windows']")
	private WebElement frames;

	@FindBy(xpath = "//span[text()='Nested Frames']")
	private WebElement nestedframes;
	@FindBy(id = "frame1")
	private WebElement frame1;

	@FindBy(xpath = "//body[text()='Parent frame']")
	private WebElement parentframes;

	public void navigateToNestedFrames() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,500)");

		sf.clickElement(frames);
		js.executeScript("arguments[0].click();", nestedframes);
		// sf.clickElement(nestedframes);
	}

	public void verifyParentFrame() {
		driver.switchTo().frame(frame1);
		String text = parentframes.getText();
		System.out.println(text);
	}
}
