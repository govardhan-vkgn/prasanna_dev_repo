package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class SelectMenuPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public SelectMenuPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;

	@FindBy(xpath = "//span[text()='Select Menu']")
	private WebElement SelectMenu;

	@FindBy(xpath = "//input[@id='react-select-5-input']")
	private WebElement SelectOption;

	@FindBy(xpath = "//input[@id='react-select-6-input']")
	private WebElement SelectTitle;

	@FindBy(xpath = "//select[@id='oldSelectMenu']")
	private WebElement oldSelectMenu;

	@FindBy(xpath = "//input[@id='react-select-7-input']")
	private WebElement multiSelect;

	@FindBy(xpath = "//select[@id='cars']")
	private WebElement standardmultiSelect;
	
	@FindBy(xpath = "//option[text()='Volvo']")
	private WebElement Selectvolvo;
	
	@FindBy(xpath = "//option[text()='Saab']")
	private WebElement SelectSaab;
	
	@FindBy(xpath = "//option[text()='Audi']")
	private WebElement SelectAudi;

	public void navigateToSelectMenuPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,1400)");
		sf.clickElement(Widgets);
		js.executeScript("arguments[0].click();", SelectMenu);
	}

	public void clickonSelectOption() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click;", SelectOption);
		Thread.sleep(2000);
		SelectOption.sendKeys("Group 1, option 1");
		Thread.sleep(2000);

		SelectOption.sendKeys(Keys.DOWN, Keys.ENTER);

		Thread.sleep(2000);

	}

	public void ClickonSelectTitle() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click;", SelectTitle);
		Thread.sleep(2000);
		SelectTitle.sendKeys("Ms.");
		Thread.sleep(2000);
		SelectTitle.sendKeys(Keys.DOWN, Keys.ENTER);
		Thread.sleep(2000);
	}

	public void selectoldSelectMenu(String val) {
		sf.selectValueFromDropDown(oldSelectMenu, val);

	}

	public void multiSelect() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click;", multiSelect);
		Thread.sleep(2000);
		multiSelect.sendKeys("Green", Keys.ENTER);
		Thread.sleep(2000);
		multiSelect.sendKeys("Blue", Keys.ENTER);
		Thread.sleep(2000);
		multiSelect.sendKeys("Black", Keys.ENTER);
		Thread.sleep(2000);
	}

	public void clickonStandardmultiselect() {
		sf.clickElement(Selectvolvo);
		sf.clickElement(SelectSaab);
		sf.clickElement(SelectAudi);

	}
}
