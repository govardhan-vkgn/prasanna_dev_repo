package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class RadioBtnPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public RadioBtnPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Elements']")
	private WebElement elements;

	@FindBy(xpath = "//span[text()='Radio Button']//parent::li")
	private WebElement radiobutton;
	
	@FindBy(xpath = "//label[text()='Yes']")
	private WebElement clickonyes;
	
	@FindBy(xpath = "//label[text()='Impressive']")
	private WebElement clickonimpressive;
	
	public void navigatetoradiobtn() {
		sf.clickElement(elements);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(100,600)");
		
		sf.clickElement(radiobutton);
	}
	public void VerifyRadiobuttonisSelected(){
		sf.clickElement(clickonyes);
		
	}
	

}
