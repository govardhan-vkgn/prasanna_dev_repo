package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class ToolsTipPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public ToolsTipPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;
	@FindBy(xpath = "//span[text()='Tool Tips']")
	private WebElement toolstippage;

	public void navigatetoToolsTipPage() throws InterruptedException {
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,1500)");
		sf.clickElement(Widgets);
		//JavascriptExecutor js1=(JavascriptExecutor) driver;
		//js1.executeScript("window.scrollBy(100,1000)");
		//sf.clickElement(toolstippage);
		js.executeScript("arguments[0].click();", toolstippage);
		Thread.sleep(2000);
	}

}
