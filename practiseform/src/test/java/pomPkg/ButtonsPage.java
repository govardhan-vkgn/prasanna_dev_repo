package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import utilitiess.SeleniumFunctions;

public class ButtonsPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public ButtonsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Elements']")
	private WebElement Elements;

	@FindBy(xpath = "//span[text()='Buttons']")
	private WebElement Buttons;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	private WebElement doubleclick;

	@FindBy(xpath = "//button[text()='Right Click Me']")
	private WebElement rightclick;

	@FindBy(xpath = "//p[text()='You have done a right click']")
	private WebElement rightclicktext;

	@FindBy(xpath = "//button[text()='Click Me']")
	private WebElement click;

	public void navigatetoButtonsPage() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,600)");
		sf.clickElement(Elements);
		Thread.sleep(2000);
		sf.clickElement(Buttons);
	}

	public void clickElements() throws InterruptedException {
		Actions action = new Actions(driver);
		action.doubleClick(doubleclick).build().perform();
		Thread.sleep(2000);
		action.contextClick(rightclick).build().perform();
		Thread.sleep(2000);
		String text = rightclicktext.getText();
		System.out.println(text);
		Thread.sleep(2000);
		sf.clickElement(click);

	}
}
