package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class AccordianPage {

	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public AccordianPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	@FindBy(xpath="//div[text()='Widgets']")
	private WebElement Widgets;
	
	@FindBy(xpath="//span[text()='Accordian']")
	private WebElement Accordian;
	@FindBy(id="section1Content")
	private WebElement loreumIpsumclick;
	
	@FindBy(id="section2Heading")
	private WebElement section2;
	
	@FindBy(id="section3Heading")
	private WebElement section3;
	
	
	public void navigateToAccordianPage() {
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,500)");
		sf.clickElement(Widgets);
		sf.clickElement(Accordian);
	}
	public void clickonloreumipsum() throws InterruptedException {
		sf.clickElement(loreumIpsumclick);
		Thread.sleep(2000);
		sf.clickElement(loreumIpsumclick);
	}
	public void clickonsection2() throws InterruptedException {
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,500)");
		sf.clickElement(section2);
		Thread.sleep(2000);
		sf.clickElement(section2);
	}
	public void clickonsection3() throws InterruptedException {
		
		sf.clickElement(section3);
		Thread.sleep(2000);
		sf.clickElement(section3);
	}
	

}
