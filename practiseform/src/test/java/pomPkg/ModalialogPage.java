package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class ModalialogPage {
	WebDriver driver;
SeleniumFunctions sf=new SeleniumFunctions();
	public ModalialogPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	@FindBy(xpath = "//div[text()='Alerts, Frame & Windows']")
	private WebElement modal;

	@FindBy(xpath = "//span[text()='Modal Dialogs']")
	private WebElement Modaldialog;
	
	@FindBy(xpath = "//button[text()='Small modal']")
	private WebElement clickonSmallModal;
	
	@FindBy(xpath = "//div[text()='This is a small modal. It has very less content']")
	private WebElement TextSmallModal;
	
	@FindBy(xpath = "//button[text()='Close']")
	private WebElement closeSmallModal;
	
	@FindBy(xpath = "//button[@class='close']")
	private WebElement close;
	@FindBy(xpath = "//button[text()='Large modal']")
	private WebElement Largemodal;
	
	@FindBy(xpath = "//button[text()='Close']")
	private WebElement CloseLargemodal;
	
	

	public void navigatetomodaldialogpage() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,1500)");
		sf.clickElement(modal);
		 Thread.sleep(2000);
		// sf.clickElement(Modaldialog);
		// js.executeScript("arguments[0].click", modal);
		js.executeScript("arguments[0].click();", Modaldialog);
		
	}
	public void clickonMadalType() throws InterruptedException {
		sf.clickElement(clickonSmallModal);
		Thread.sleep(2000);
		String text=TextSmallModal.getText();
		System.out.println(text);
		Thread.sleep(2000);
		sf.clickElement(closeSmallModal);		
		Thread.sleep(2000);
		sf.clickElement(clickonSmallModal);
		Thread.sleep(2000);
		sf.clickElement(close);
		Thread.sleep(2000);
		sf.clickElement(Largemodal);
		Thread.sleep(2000);
		sf.clickElement(CloseLargemodal);
		Thread.sleep(2000);
		sf.clickElement(Largemodal);
		Thread.sleep(2000);
		sf.clickElement(close);
		
		
	}
}
