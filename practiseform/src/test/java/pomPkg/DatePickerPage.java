package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class DatePickerPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public DatePickerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;

	@FindBy(xpath = "//span[text()='Date Picker']")
	private WebElement datepicker;

	@FindBy(id = "datePickerMonthYearInput")
	private WebElement selectdate;

	@FindBy(xpath = "//input[@id='dateAndTimePickerInput']")
	private WebElement dateAndTimePickerInput;

	public void navigateToDatePicker() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,500)");
		sf.clickElement(Widgets);
		sf.clickElement(datepicker);
	}

	public void selectDate() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", selectdate);
		selectdate.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		// selectdate.click();
		Thread.sleep(2000);
		selectdate.clear();
		Thread.sleep(2000);
		selectdate.sendKeys("2/12/2021");
		Thread.sleep(2000);
		selectdate.sendKeys(Keys.ENTER);
	}

	public void clickonDateandTimePickerInput() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click", dateAndTimePickerInput);
		Thread.sleep(2000);
		dateAndTimePickerInput.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		Thread.sleep(2000);
		dateAndTimePickerInput.clear();
		Thread.sleep(2000);
		dateAndTimePickerInput.sendKeys("February 12,2021 17:00 PM");
		Thread.sleep(2000);
		/*
		 * sf.clickElement(dateAndTimePickerInput);
		 * sf.selectValueFromDropDown(selectmonth, val);
		 * sf.selectValueFromDropDown(selectyear, val1); sf.clickElement(clickondate);
		 * sf.clickElement(selecttime);
		 */
	}
}
