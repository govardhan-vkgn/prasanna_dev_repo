package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class AutoCompletePage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public AutoCompletePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;

	@FindBy(xpath = "//span[text()='Auto Complete']")
	private WebElement Autocompletepage;

	@FindBy(xpath = "//input[@id='autoCompleteMultipleInput']")
	private WebElement TypeMultipleColorNames;

	@FindBy(xpath = "//input[@id='autoCompleteSingleInput']")
	private WebElement Typesinglecolor;

	public void navigateToAutoCompletePage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,500)");
		sf.clickElement(Widgets);
		sf.clickElement(Autocompletepage);
	}

	public void enterMultipleColorNames(String val)  {
		TypeMultipleColorNames.sendKeys("red", ",", "black");
	
		
	}
	public void enterSingleColorNames(String val) {
		
		Typesinglecolor.sendKeys("pink", ",", "blue");

	}
}
