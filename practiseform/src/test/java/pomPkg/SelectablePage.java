package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class SelectablePage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public SelectablePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	@FindBy(xpath="//div[text()='Interactions']")
	private WebElement Interactions;
	
	@FindBy(xpath="//span[text()='Selectable']")
	private WebElement Selectable;
	
	@FindBy(xpath="//li[text()='Cras justo odio']")
	private WebElement Crasjustoodio;
	
	@FindBy(xpath="//li[text()='Dapibus ac facilisis in']")
	private WebElement Dapibusac;
	
	@FindBy(xpath="//a[text()='Grid']")
	private WebElement Grid;	
	
	@FindBy(xpath="//li[text()='One']")
	private WebElement clickonOne;
	
	@FindBy(xpath="//li[text()='Two']")
	private WebElement clickonTwo;
	
	@FindBy(xpath="//li[text()='Nine']")
	private WebElement clickonNine;
	
	public void navigatetoSelectablePage() {
		JavascriptExecutor js=(JavascriptExecutor) driver; 
		js.executeScript("window.scrollBy(100,2000)");
		js.executeScript("arguments[0].click();",Interactions );
		//sf.clickElement(Interactions);
		js.executeScript("arguments[0].click();",Selectable );
		
	}
	
	public void clickonList() throws InterruptedException {
		sf.clickElement(Crasjustoodio);
		Thread.sleep(2000);
		sf.clickElement(Dapibusac);
		Thread.sleep(2000);
		sf.clickElement(Crasjustoodio);
	}
	
	public void clickonGrids() throws InterruptedException {
		JavascriptExecutor js=(JavascriptExecutor) driver; 
		sf.clickElement(Grid);
		Thread.sleep(2000);
		sf.clickElement(clickonOne);
		Thread.sleep(2000);
		sf.clickElement(clickonTwo);
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();",clickonNine );
		//sf.clickElement(clickonNine);
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();",clickonTwo );
		
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();",clickonTwo );
		
	}
	
}
