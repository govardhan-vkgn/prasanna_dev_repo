package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class SliderPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public SliderPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;

	@FindBy(xpath = "//span[text()='Slider']")
	private WebElement slider;
	
	@FindBy(xpath="//input[@class='range-slider range-slider--primary']//ancestor::div[2]")
	private WebElement slidervalue;
	
	
	public void navigatetosliderpage() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(100,500)");
		sf.clickElement(Widgets);
		sf.clickElement(slider);
	}
	
	public void enterSliderValue() throws InterruptedException {
		Actions action= new Actions(driver);
		action.dragAndDropBy(slidervalue,5,0).perform();
		Thread.sleep(2000);
		action.dragAndDropBy(slidervalue,15,0).perform();
		Thread.sleep(2000);
		action.dragAndDropBy(slidervalue,25,0).perform();
		Thread.sleep(2000);
		
	}

}
