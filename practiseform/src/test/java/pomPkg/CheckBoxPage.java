package pomPkg;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class CheckBoxPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public CheckBoxPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Elements']")
	private WebElement elements;

	@FindBy(xpath = "//span[text()='Check Box']")
	private WebElement checkbox;

	@FindBy(xpath = "//span[@class='rct-checkbox']")
	private WebElement clickcheckbox;
	
	@FindBy(xpath = "//div[@id='result']/span[1]")
	private WebElement resulttext;

	@FindBy(xpath = "//button[@title='Expand all']")
	private WebElement expandthecheckbox;
	
	@FindBy(xpath = "//button[@title='Collapse all']")
	private WebElement collapsecheckbox;

	public void navigateToCheckBoxPage() {
		sf.clickElement(elements);
		sf.clickElement(checkbox);
	}

	public void verifyhomeisselected() {
		sf.clickElement(clickcheckbox);
		clickcheckbox.isSelected();
		//clickcheckbox.isSelected();

	}

	public void verifytextisprinted() {
		String text= resulttext.getText();
		System.out.println(text);

	}

	public void verifyallcheckboxesexpanded() {
		sf.clickElement(expandthecheckbox);

	}

	public void verifyallcheckboxescollapsed() {
		sf.clickElement(collapsecheckbox);

	}

}
