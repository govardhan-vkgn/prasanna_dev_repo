package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class TabsPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public TabsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Widgets']")
	private WebElement Widgets;
	@FindBy(xpath = "//span[text()='Tabs']//parent::li")
	private WebElement Tabspage;
	
	public void navigatetoTabsPage() {
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,600)");
		sf.clickElement(Widgets);
		JavascriptExecutor js1=(JavascriptExecutor) driver;
		js1.executeScript("window.scrollBy(100,600)");
		sf.clickElement(Tabspage);
	}
}
