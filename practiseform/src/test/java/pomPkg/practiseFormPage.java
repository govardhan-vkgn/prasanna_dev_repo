package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class practiseFormPage {
	WebDriver driver;
	SeleniumFunctions se = new SeleniumFunctions();

	public practiseFormPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "firstName")
	private WebElement firstName;
	@FindBy(id = "lastName")
	private WebElement LastName;

	@FindBy(xpath = "//input[@placeholder='name@example.com']")
	private WebElement Emailid;

	@FindBy(xpath = "//label[text()='Male']")
	private WebElement GenderMale;

	@FindBy(xpath = "//label[text()='Female']")
	private WebElement GenderFeMale;

	@FindBy(xpath = "//input[@placeholder='Mobile Number']")
	private WebElement MobileNumber;

	@FindBy(xpath = "//input[@id='dateOfBirthInput']")
	private WebElement dateOfBirthInput;// click

	@FindBy(xpath = "//select[@class='react-datepicker__month-select']")
	private WebElement month;

	@FindBy(xpath = "//select[@class='react-datepicker__year-select']")
	private WebElement SelectYear;

	@FindBy(xpath = "//div[@class='react-datepicker__day react-datepicker__day--005']")
	private WebElement SelectDate;

	@FindBy(xpath = "//label[@for='hobbies-checkbox-1']")
	private WebElement HobbiesSports;

	@FindBy(xpath = "//label[@for='hobbies-checkbox-2']")
	private WebElement HobbiesReading;

	@FindBy(xpath = "//label[@for='hobbies-checkbox-3']")
	private WebElement HobbiesMusic;

	@FindBy(xpath = "//textarea[@placeholder='Current Address']")
	private WebElement CurrentAddress;

	@FindBy(xpath = "//input[@id='subjectsInput']")

	private WebElement subject;

	@FindBy(xpath = "//input[@id='react-select-3-input']")

	private WebElement city1;

	@FindBy(xpath = "//input[@id='react-select-4-input']")

	private WebElement city2;

	public void enterFirstName(String val) {
		se.enterValueIntoTextField(firstName, val);
	}

	public void enterLastName(String val) {
		se.enterValueIntoTextField(LastName, val);
		System.out.println();
	}

	public void enterEmailid(String val) {
		se.enterValueIntoTextField(Emailid, val);
	}

	public void verifythegendertype(String val) {
		if (val.equalsIgnoreCase("male")) {
			se.clickElement(GenderMale);
		} else if (val.equalsIgnoreCase("female")) {
			se.clickElement(GenderFeMale);

		}
	}

	public void enterMobileno(String val) {
		se.enterValueIntoTextField(MobileNumber, val);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(100,400)");

	}

	public void selectDateofBirth(String val,String val2) {
		se.clickElement(dateOfBirthInput);
		se.selectValueFromDropDown(month,val);
		se.selectValueFromDropDown(SelectYear,val2);
		se.clickElement(SelectDate);
	}

	public void enterSubjectName(String val) {
		se.enterValueIntoTextField(subject, val);
	}

	public void VerifyHobbiesisselected(String val) {
		if (val.equalsIgnoreCase("sports")) {
			se.clickElement(HobbiesSports);
		} else if (val.equalsIgnoreCase("reading")) {
			se.clickElement(HobbiesReading);
		}
	}

	public void VerifyFileisSelected() {
		// firstName.sendkeys();
	}

	public void enterCurrentAddress(String val) {
		se.enterValueIntoTextField(CurrentAddress, val);
	}

	public void enterstate(String val) throws InterruptedException {
		Thread.sleep(2000);

		city1.sendKeys("Haryana");

		Thread.sleep(2000);

		city1.sendKeys(Keys.DOWN, Keys.ENTER);

		Thread.sleep(2000);
	}

	public void entercity(String val) throws InterruptedException {
		Thread.sleep(2000);

		city2.sendKeys("Karnal");

		Thread.sleep(2000);

		city2.sendKeys(Keys.DOWN, Keys.ENTER);

		Thread.sleep(2000);
	}

}
