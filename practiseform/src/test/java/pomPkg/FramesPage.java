package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class FramesPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public FramesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Alerts, Frame & Windows']")
	private WebElement Frames;

	@FindBy(xpath = "//span[text()='Frames']")
	private WebElement framespage;

	@FindBy(id = "frame1")
	private WebElement switchtoframe;

	@FindBy(id = "sampleHeading")
	private WebElement samplepagetext;

	public void navigateTOFramesPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100, 500)");
		sf.clickElement(Frames);
		sf.clickElement(framespage);

	}

	public void frames() {

		driver.switchTo().frame(switchtoframe);

		String framestext = samplepagetext.getText();

		System.out.println(framestext);
	}

}
