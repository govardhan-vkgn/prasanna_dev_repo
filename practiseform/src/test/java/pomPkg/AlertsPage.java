package pomPkg;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class AlertsPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public AlertsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Alerts, Frame & Windows']")
	private WebElement alertsframe;

	@FindBy(xpath = "//span[text()='Alerts']")
	private WebElement alertspage;

	@FindBy(xpath = "//button[text()='Click me']")
	private WebElement simplealert;
	
	@FindBy(id = "timerAlertButton")
	private WebElement timerAlertButton;
	
	@FindBy(id = "confirmButton")
	private WebElement conformationalert;
	
	@FindBy(id = "promtButton")
	private WebElement promtalert;
	

	public void navigateToAlertsPage() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(100,500)");
		sf.clickElement(alertsframe);
		sf.clickElement(alertspage);
	}

	public void clickonsimplealert() {
		sf.clickElement(simplealert);
		Alert click=driver.switchTo().alert();	
         click.accept();
         
	}
	/*public void clickontimerAlertButton() {
		sf.clickElement(timerAlertButton);
		driver.switchTo().alert().accept();
         
	}*/

	public void clickonconformationAlert() {
		sf.clickElement(conformationalert);
		driver.switchTo().alert().accept();
	}
	
	public void clickonPromptAlert() throws InterruptedException {
		sf.clickElement(promtalert);
		driver.switchTo().alert().sendKeys("prasanna");
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
	}

}
