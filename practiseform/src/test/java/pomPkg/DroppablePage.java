package pomPkg;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class DroppablePage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public DroppablePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	@FindBy(xpath="//div[text()='Interactions']")
	private WebElement Interactions;
	
	@FindBy(xpath="//span[text()='Droppable']")
	private WebElement Droppable;
	
	@FindBy(id="draggable")
	private WebElement draggable;
	
	@FindBy(id="droppable")
	private WebElement droppable;
	
	public void navigatetoDroppablePage() {
		JavascriptExecutor js=(JavascriptExecutor) driver; 
		js.executeScript("window.scrollBy(100,2000)");
		js.executeScript("arguments[0].click();",Interactions );
		//sf.clickElement(Interactions);
		js.executeScript("arguments[0].click();", Droppable );
		
	}
	
	public void clickonDroppable() {
		Actions action=new Actions(driver);
		action.dragAndDrop(draggable, droppable).build().perform();
		
	}
}
