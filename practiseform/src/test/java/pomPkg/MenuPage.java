package pomPkg;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilitiess.SeleniumFunctions;

public class MenuPage {
	WebDriver driver;
	SeleniumFunctions sf = new SeleniumFunctions();

	public MenuPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	@FindBy(xpath="//div[text()='Widgets']")
	private WebElement Widgets;
	
	@FindBy(xpath="//span[text()='Menu']")
	private WebElement Accordian;
	@FindBy(xpath="//div[text()='Menu']")
	private WebElement title;
	
	@FindBy(xpath="//a[text()='Main Item 2']")
	private WebElement MainItem2;
	
	@FindBy(xpath="//a[text()='SUB SUB LIST �']")
	private WebElement SUBLIST;
	
	public void navigatetoMenuPage() {
		JavascriptExecutor js=(JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(100,2000)");
		sf.clickElement(Widgets);
		js.executeScript("arguments[0].click();",Accordian);
		
	}
	public void clickonMainmenu2() throws InterruptedException {
		String text = title.getText();
		System.out.println(text);
		Thread.sleep(2000);
		Actions action=new Actions(driver);
		action.moveToElement(MainItem2).perform();
		Thread.sleep(2000);
		action.moveToElement(SUBLIST).perform();
	}
}
