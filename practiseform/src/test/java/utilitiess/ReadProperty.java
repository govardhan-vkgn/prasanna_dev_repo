package utilitiess;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperty {
	

		Properties prop;

		public ReadProperty() throws IOException {
			FileInputStream fis = new FileInputStream("C:\\Users\\dumne\\git\\testngproject\\Formpractiseid\\config\\configii.properties");
			prop = new Properties();
			prop.load(fis);

		}

		public String getUrl() {
			return prop.getProperty("url");
		}

		public String getFirstName() {
			return prop.getProperty("firstname");
		}

		public String getLastName() {
			return prop.getProperty("lastname");
		}

		public String getEmailid() {
			return prop.getProperty("email");
		}

		public String getgender() {
			return prop.getProperty("gender");
		}

		public String getMobileno() {
			return prop.getProperty("mobileno");
		}

		public String getSubject() {
			return prop.getProperty("subject");
		}

		public String getHobbies() {
			return prop.getProperty("hobbies");
		}

		public String getAddress() {
			return prop.getProperty("address");
		}

		public String getstate() {
			return prop.getProperty("state");
		}

		public String getcity() {
			return prop.getProperty("city");
		}
		public String getusername() {
			return prop.getProperty("tc2_username");
		}
		public String getmulticolors() {
			return prop.getProperty("mulicolors");
		}
		public String getsinglecolors() {
			return prop.getProperty("singlecolor");
		}


}
