package utilitiess;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SeleniumFunctions {

	public void clickElement(WebElement ele) {
		ele.click();
	}

	public void enterValueIntoTextField(WebElement ele, String val) {
		ele.sendKeys(val);
		
	}

	public void selectValueFromDropDown(WebElement ele,String val) {
		Select sc=new Select(ele);
		sc.selectByVisibleText(val);
		
		
		
	}

}