package testPkg;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.IInvokedMethod;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


import utilitiess.ReadProperty;

public class BaseClass {
	public WebDriver driver;

	public ExtentReports extent;
	public ExtentTest extentTest;

	@BeforeTest
	public void setExtent() {
		extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/ExtentReport.html", true);
		extent.addSystemInfo("Host Name", "CRM on Mac");
		extent.addSystemInfo("User Name", "CRM Automation Test Report");
		extent.addSystemInfo("Environment", "");
	}

	@AfterTest
	public void endReport() {
		extent.flush();
		extent.close();
	}

	// 20220129055110

	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());

		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots"
		// under src folder
		String destination = System.getProperty("user.dir") + "/test-output/Images/" + screenshotName + dateName
				+ ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

	@BeforeMethod
	public void launchApplication(final ITestContext testContext,IInvokedMethod method) throws IOException, InterruptedException {
		extentTest = extent.startTest(testContext.getName());
		System.out.println(testContext.getName());
		ReadProperty config = new ReadProperty();
		String url = config.getUrl();
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dumne\\git\\testngproject\\prasanna_dev_repo\\practiseform\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		 driver.get(url);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		System.out.println( method.getTestMethod().getMethodName());

	}
	/*
	 * @AfterMethod public void closeBrowser() { driver.close();
	 * 
	 * }
	 */
	@AfterMethod
	public void tearDown(ITestResult result,IInvokedMethod method) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS " + method.getTestMethod().getMethodName()); // to add name in extent report
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in
																							// extent report

			String screenshotPath = getScreenshot(driver, method.getTestMethod().getMethodName());
			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(screenshotPath)); // to add screenshot in extent
																							// report

		} else if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case SKIPPED IS " + method.getTestMethod().getMethodName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case PASSED IS " + method.getTestMethod().getMethodName());

			String screenshotPath = getScreenshot(driver, method.getTestMethod().getMethodName());
			extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(screenshotPath)); // to add screenshot in extent

		}

		extent.endTest(extentTest); // ending test and ends the current test and prepare to create html report
		driver.quit();
	}
}

