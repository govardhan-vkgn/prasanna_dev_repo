package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.RadioBtnPage;
import utilitiess.ReadProperty;

public class RadioBtnTest extends BaseClass {
	@Test
	public void verifyRadioBtnTest() throws IOException{
		ReadProperty prop = new ReadProperty();
		RadioBtnPage rbp = new RadioBtnPage(driver);
		rbp.navigatetoradiobtn();
		rbp.VerifyRadiobuttonisSelected();

	}

}
