package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.SelectablePage;
import utilitiess.ReadProperty;

public class SelectableTest extends BaseClass {
	@Test
	public void verifyListandGridSeleced() throws IOException, InterruptedException {
		ReadProperty prop = new ReadProperty();
		SelectablePage stp = new SelectablePage(driver);
		stp.navigatetoSelectablePage();
		stp.clickonList();
		stp.clickonGrids();
	}
}