package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;
import pomPkg.SelectMenuPage;
import utilitiess.ReadProperty;

public class SelectMenuTest extends BaseClass {
	@Test
	public void verifyMenuisSelected() throws IOException, InterruptedException {
		ReadProperty prop = new ReadProperty();
		SelectMenuPage smp = new SelectMenuPage(driver);
		smp.navigateToSelectMenuPage();
		smp.clickonSelectOption();
		smp.ClickonSelectTitle();
		smp.selectoldSelectMenu("Green");
		smp.multiSelect();
		smp.clickonStandardmultiselect();
	}
}
