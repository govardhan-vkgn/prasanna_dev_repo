package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.TestBoxPage;
import utilitiess.ReadProperty;

public class TestBoxTest extends BaseClass {

	@Test
	public void verifyDetailsEnteredinTextBox() throws IOException {
		ReadProperty prop = new ReadProperty();
		String uname = prop.getusername();
		String email = prop.getEmailid();
		TestBoxPage tbp = new TestBoxPage(driver);
		tbp.navigateTOTextBoxPage();
		tbp.enterFullname(uname);
		tbp.enterEmalid(email);

	}

}
