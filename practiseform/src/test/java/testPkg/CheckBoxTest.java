package testPkg;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.Test;

import pomPkg.CheckBoxPage;
import utilitiess.ReadProperty;

public class CheckBoxTest extends BaseClass {
		@Test
		public void verifythecheckboxisselected() throws IOException, InterruptedException {
			ReadProperty prop = new ReadProperty();
			CheckBoxPage cbp = new CheckBoxPage(driver);
			cbp.navigateToCheckBoxPage();
			cbp.verifyhomeisselected();
			//String text=cbp. verifytextisprinted();
			cbp.verifytextisprinted();
			//Assert.assertEquals("You have selected :",text );
			cbp.verifyallcheckboxesexpanded();
			Thread.sleep(2000);
			cbp.verifyallcheckboxescollapsed();
		}
	}


