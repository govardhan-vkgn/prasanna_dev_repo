package testPkg;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.Test;

import pomPkg.practiseFormPage;
import utilitiess.ReadProperty;

public class practiseFormTest extends BaseClass{
	@Test
	public void veriftyPractiseFormApllicatioSubmitted() throws IOException, InterruptedException {
		ReadProperty read=new ReadProperty();
		String fname=read.getFirstName();
		String lname=read.getLastName();
		String email=read.getEmailid();
		String gender=read.getgender();
		String mobileno=read.getMobileno();
		String subject=read.getSubject();
		String hobbies=read. getHobbies();
		String caddress=read.getAddress();
		String state=read.getstate();
		String city=read.getcity();
		practiseFormPage praform = new practiseFormPage(driver);
		praform.enterFirstName(fname);
		praform.enterLastName(lname);
		praform.enterEmailid(email);
		praform.verifythegendertype(gender);
		praform.enterMobileno(mobileno);
		praform.selectDateofBirth("February","2020");
		praform.enterSubjectName(subject);
		praform.VerifyHobbiesisselected(hobbies);
		praform.enterCurrentAddress(caddress);
		praform.enterstate(state);
		praform.entercity(city);
		Assert.assertTrue(true);
		
	}
}