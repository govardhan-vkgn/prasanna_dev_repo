package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.NestedFramesPage;
import utilitiess.ReadProperty;

public class NestedFrameTest extends BaseClass{
	@Test
	public void verifyNestedFrames() throws IOException {
		ReadProperty prop=new ReadProperty();
		NestedFramesPage nfp=new NestedFramesPage(driver);
		nfp.navigateToNestedFrames();
		nfp.verifyParentFrame();
	}
	
}
