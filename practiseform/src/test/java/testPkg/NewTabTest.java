package testPkg;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pomPkg.AlertsPage;
import pomPkg.NewtabPage;
import utilitiess.ReadProperty;
import utilitiess.SeleniumFunctions;

public class NewTabTest extends BaseClass {
	@Test
	public void verifyNewTabIsOpened() throws IOException {
		ReadProperty prop = new ReadProperty();
		NewtabPage ntb = new NewtabPage(driver);
		ntb.navigatetowindowspage();
		ntb.VerifyNewTabisOpened();

	}
}
