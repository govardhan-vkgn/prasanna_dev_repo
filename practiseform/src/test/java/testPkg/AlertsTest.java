package testPkg;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import pomPkg.AlertsPage;
import utilitiess.ReadProperty;

public class AlertsTest extends BaseClass {

	@Test
	public void simpleAlert() throws IOException, InterruptedException {
		ReadProperty prop = new ReadProperty();
		AlertsPage alp = new AlertsPage(driver);
		alp.navigateToAlertsPage();
		alp.clickonsimplealert();
		Thread.sleep(2000);
		//alp.clickontimerAlertButton();
		Thread.sleep(2000);
		alp.clickonconformationAlert();
		Thread.sleep(2000);
		alp.clickonPromptAlert();
	}

}
