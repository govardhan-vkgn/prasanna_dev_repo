package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.AutoCompletePage;
import utilitiess.ReadProperty;

public class AutoCompleteTest extends BaseClass {
	@Test
	public void verifyAutocomplete() throws IOException, InterruptedException {
		ReadProperty prop = new ReadProperty();
		String mucolor=prop.getmulticolors();
		String sicolor=prop.getsinglecolors();
		AutoCompletePage acp = new AutoCompletePage(driver);
		acp.navigateToAutoCompletePage();
		Thread.sleep(2000);
		acp.enterMultipleColorNames(mucolor);
		Thread.sleep(2000);
		acp.enterSingleColorNames(sicolor);
	}
}
