package testPkg;

import java.io.IOException;

import org.testng.annotations.Test;

import pomPkg.FramesPage;
import utilitiess.ReadProperty;

public class FramesTest extends BaseClass{
	@Test
	public void frames() throws IOException {
		ReadProperty prop=new ReadProperty();
		FramesPage fp=new FramesPage(driver);
		fp.navigateTOFramesPage();
		fp.frames();
		
	}

}
