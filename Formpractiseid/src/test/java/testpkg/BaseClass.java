package testpkg;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utilities.ReadProperty;

public class BaseClass {
	public WebDriver driver;

	@BeforeMethod
	public void launchApplication() throws IOException, InterruptedException {
		ReadProperty config = new ReadProperty();
		String url = config.getUrl();
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dumne\\git\\testngproject\\Formpractiseid\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		 driver.get(url);
		//driver.get("https://demoqa.com/text-box");

		driver.manage().window().maximize();
		Thread.sleep(5000);

	}
	/*
	 * @AfterMethod public void closeBrowser() { driver.close();
	 * 
	 * }
	 */
}
