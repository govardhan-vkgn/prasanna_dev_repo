package pomPKG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.SeleniumFunctions;

public class TestBoxPage {
	WebDriver driver;
	SeleniumFunctions sf=new SeleniumFunctions();
	public TestBoxPage (WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);	
	}
	 @FindBy(xpath="//input[@id='userName']")
	 private WebElement username;
	 @FindBy(xpath="//input[@placeholder='name@example.com']")
	 private WebElement emailid;
	
	 
	 public void enterFullname(String val) {
		 sf.enterValueIntoTextField(username,val);
		 //username.sendKeys("prasanna");
	 }
	 public void enterEmalid(String val) {
		 sf.enterValueIntoTextField(emailid,val);
	 }
	 
	}


