package pomPKG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Form {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//driver.get("https://demoqa.com/automation-practice-form");
		driver.get("https://demoqa.com/text-box");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		//driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("prasanna");
		Thread.sleep(2000);
		driver.findElement(By.id("firstName")).sendKeys("prasanna");
		Thread.sleep(2000);
		driver.findElement(By.id("firstName")).clear();
		Thread.sleep(2000);
		driver.findElement(By.id("firstName")).sendKeys("prasanna");
		Thread.sleep(2000);
		// radio button
		driver.findElement(By.xpath("//label[text()='Male']")).click();
		boolean state = driver.findElement(By.name("gender")).isSelected();
		System.out.println(state);
		// dropdown
		driver.findElement(By.xpath("//input[@id='dateOfBirthInput']")).click();
		WebElement drpdwn = driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"));
		Select se = new Select(drpdwn);
		se.selectByVisibleText("April");
		WebElement drpdwn1 = driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"));
		Select se1 = new Select(drpdwn1);
		se1.selectByValue("1901");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='react-datepicker__day react-datepicker__day--004']")).click();
		driver.findElement(By.xpath("//input[@id='subjectsInput']")).sendKeys("jjjdj");
		Thread.sleep(2000);
	
		driver.findElement(By.xpath("(//div[@class=' css-1wa3eu0-placeholder'])[1]")).click();
	
		driver.findElement(By.xpath("//img[@title='Ad.Plus Advertising']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//label[text()='Sports']")).click();
		Thread.sleep(2000);
		//driver.findElement(By.xpath("//input[@id='uploadPicture']"));
		
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).submit();
	}

}
